<?php
require '../../../zb_system/function/c_system_base.php';
require '../../../zb_system/function/c_system_admin.php';
$zbp->Load();
$action = 'root';
$name = 'Accelerator';
if (!$zbp->CheckRights($action)) {
	$zbp->ShowError(6);
	die();
}
if (!$zbp->CheckPlugin($name)) {
	$zbp->ShowError(48);
	die();
}

$blogtitle = '高负载_网站加速器';
require $blogpath . 'zb_system/admin/admin_header.php';
require $blogpath . 'zb_system/admin/admin_top.php';
$ZC_VIEWNUMS_TURNOFF = $zbp->option['ZC_VIEWNUMS_TURNOFF'];
$ZC_RUNINFO_DISPLAY = $zbp->option['ZC_RUNINFO_DISPLAY'];
if($ZC_VIEWNUMS_TURNOFF===false) $ZC_VIEWNUMS_TURNOFF=0;
if($ZC_RUNINFO_DISPLAY===false) $ZC_RUNINFO_DISPLAY=0;
//var_dump($ZC_VIEWNUMS_TURNOFF);
$act = GetVars('act');
if (empty($act)) $act = 'config';
if (count($_POST) > 0) {
	if (function_exists('CheckIsRefererValid')) CheckIsRefererValid();
	$zbp->option['ZC_VIEWNUMS_TURNOFF'] = (bool)$_POST['ZC_VIEWNUMS_TURNOFF'];
	$zbp->option['ZC_RUNINFO_DISPLAY'] = (bool)$_POST['ZC_RUNINFO_DISPLAY'];
	$zbp->SaveOption();
	$zbp->SetHint('good', '保存成功！');
	Redirect('./main.php');
}
?>
<div id="divMain">
	<div class="divHeader"><?php echo $blogtitle; ?></div>
	<div class="SubMenu">
		<div class="SubMenu">
			<a href="?act=config"><span>基本设置</span></a>
			<a href="?act=explain"><span>加速建议</span></a>
		</div>
	</div>
	<div id="divMain2">
		<?php if ($act == 'config') { ?>
			<form id="form1" name="form1" method="post">
				<?php if (function_exists('CheckIsRefererValid')) {
					echo '<input type="hidden" name="csrfToken" value="' . $zbp->GetCSRFToken() . '">';
				} ?>
				<table border="1" class="tableFull tableBorder">
					<tr>
						<th>
							<p align='left'>选项</p>
						</th>
						<th>
							<p>配置</p>
						</th>
					</tr>

					<tr>
						<td>
							<p align='left'><b>文章浏览量</p>
						</td>
						<td>
							<p>
								<select name="ZC_VIEWNUMS_TURNOFF" id="ZC_VIEWNUMS_TURNOFF" class="noPicWay">
									<option value="1" <?php echo Accelerator_Selectdefault($ZC_VIEWNUMS_TURNOFF, '1'); ?>>关闭</option>
									<option value="0" <?php echo Accelerator_Selectdefault($ZC_VIEWNUMS_TURNOFF, '0'); ?>>开启</option>
								</select></b><br><br>
								用户每刷新一次就导致数据库频繁写入，关闭它，提高服务器性能，<br>关闭之后把模板里关于浏览量标签<font color="red">{$article.ViewNums}</font>批量替换成随机数，<br>虚拟显示浏览量<font color="red">{mt_rand('100','99999')}</font>
							</p>
						</td>
					</tr>
					<tr>
						<td>
							<p align='left'><b>程序执行时间</p>
						</td>
						<td>
							<p>
								<select name="ZC_RUNINFO_DISPLAY" id="ZC_RUNINFO_DISPLAY" class="noPicWay">
									<option value="0" <?php echo Accelerator_Selectdefault($ZC_RUNINFO_DISPLAY, '0'); ?>>关闭</option>
									<option value="1" <?php echo Accelerator_Selectdefault($ZC_RUNINFO_DISPLAY, '1'); ?>>开启</option>
								</select></b><br><br>
								底部最后一行程序执行时间，它会计算程序当前代码执行的效率，关闭它，可减少CPU计算
							</p>
						</td>
					</tr>



					<tr>
						<td>
						</td>
						<td><input name="" type="Submit" class="button" value="保存设置" /></td>
					</tr>

				</table>

			</form>
		<?php }
		if ($act == 'explain') { ?>
			<table border="1" class="tableFull tableBorder">
				<tr>
					<td>
						<div align='left'>
							<p><b>加速建议：</b></p>
						</div>
					</td>
				</tr>
				<tr>
					<td>
						<div align='left'>
							<p>1、CSS，JS，这些文件建议引用大厂的公共库，节省带宽，速度又快。比如字节公共库：https://cdn.bytedance.com/</p>
							<p>2、图片是非常占用并发及带宽的，无关紧要的图片尽量引用外部大厂图片，比如列表缩略图就可以引用优酷，百度的。</p>
							<p>3、同一页面尽量减少调用多处内容，在同一页面每调用一处信息就多执行一条SQL，多条SQL将会占用很多服务器性能资源，导致经常CPU，内存爆满。</p>
							<p>4、如何处理需要调用多处信息，生成txt静态文件，<font color="red">file_get_contents</font>读入内存，占用资源又少，速度又快。</p>
							<p>5、为什么要关闭文章浏览量，使用虚拟数据，我们做网站总是想最大化保证服务器稳定正常运行，总不能为了一个无关紧要的字段数量影响网站打不开吧。</p>
							<p>6、对数据库搜索是非常占用资源的。建议删除搜索功能代码，使用百度站内搜索：https://www.baidu.com/s?si=你的域名&ct=2097152&wd=关键词</p>
						</div>
					</td>
				</tr>
			</table>
		<?php } ?>
	</div>
</div>

<?php
require $blogpath . 'zb_system/admin/admin_footer.php';
RunTime();
?>