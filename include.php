<?php
#注册插件
RegisterPlugin('Accelerator', 'ActivePlugin_Accelerator');

function ActivePlugin_Accelerator()
{
	Add_Filter_Plugin('Filter_Plugin_Index_Begin', 'Accelerator_Index_Begin');
}
function Accelerator_Index_Begin()
{
	global $zbp;
}
function InstallPlugin_Accelerator()
{
	global $zbp;
}
function UninstallPlugin_Accelerator()
{
	global $zbp;
	$zbp->option['ZC_VIEWNUMS_TURNOFF'] = false;
	$zbp->option['ZC_RUNINFO_DISPLAY'] = true;
	$zbp->SaveOption();
	$zbp->DelConfig('Accelerator');
}
//判断下拉选项默认值
function Accelerator_Selectdefault($selectedOption, $option_value)
{
	return strtolower($selectedOption) === strtolower($option_value) ? 'selected="selected"' : '';
}
